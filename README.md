# README #

테루바 안드로이드 저장소입니다

## 어플리케이션 기본 설정
    - 네이밍은 일단 Teluva로 함. 정해지면 변경 예정 
    - minSdkVersion 은 jellybean(API16)으로 설정. 
      - Android Studio기준 점유율 96%, 네이버 기준 점유율 99%. 국내 사용자는 모두 커버한다고 봐도 됨.

## 기본적으로 사용할 라이브러리 & 적용 기술
    - 앱 아키텍쳐 : MVP
    - 네트워크(HTTP & HTTP/2) 처리 : OkHttp3
    - 네트워크(REST API) 처리 : Retrofit2
    - 이미지 처리 : Glide
    - 로컬 데이터베이스 : Realm
    - Baas : Firebase
    - 이미지 리소스 : Vector Drawable
    - 색상은 일단 Material Design 기준으로 적용
## 적용할 외부 솔루션
    - 앱 크래시 리포팅 서비스 : Fabric Crashlytics
    - 앱 사용 패턴 분석 서비스 : Fabric Answer
## 적용 고민중인 것들
    - 언어 : 코틀린
    - Reactive : RxJava
    - 테스트 라이브러리 : Roboletric